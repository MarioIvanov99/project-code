#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QTimer>
#include <QBoxLayout>
#include "widget.h"

class mainwindow: public QWidget
    {
    public:


    // constructor / destructor
    mainwindow(QWidget *parent);
    ~mainwindow();

    // visual hierarchy

    // window layout
    QBoxLayout *windowLayout;

    // beneath that, the main widget
    widget *cubeWidget;
    // and a slider for the number of vertices
    QSlider *nVerticesSliderY; //Used to rotate the camera in the Y axis

    // a timer
    QTimer *ptimer;

    // resets all the interface elements
    void ResetInterface();
    };

#endif
