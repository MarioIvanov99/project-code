#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QOpenGLFunctions>
#include <QOpenGLFunctions_2_1>
#include <QRandomGenerator>
#include <QGLWidget>
#include <QKeyEvent>
#include <QTime>
#include <GL/glut.h>

/*
// The variables used here are better explained in the main portion of the code
//
*/

#define HEIGHT 100
#define WIDTH 100
#define DIRECTIONS 50

typedef struct vertexStruct {
  float x, y, z;
  float nx, ny, nz;
} vertexStruct;

class widget: public QGLWidget, protected QOpenGLFunctions_2_1
    { //

    Q_OBJECT

    public:
    widget(QWidget *parent);

    public slots:
        // called by the timer in the main window
    void updateAngle();



    protected:
    // called when OpenGL context is set up
    void initializeGL(){
        initializeOpenGLFunctions();
        setMouseTracking(true);
        QCursor c = cursor();
        c.setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));
        c.setShape(Qt::BlankCursor);
        setCursor(c);
        setFocusPolicy(Qt::StrongFocus);
        glClearColor(0.6, 0.6, 0.6, 0.0);
        fps.start();
    }
    // called every time the widget is resized
    void resizeGL(int w, int h) override;
    // called every time the widget needs painting
    void paintGL() override;


    private:

    void plane(); //creates a 5x5 plane along x-z
    int getVertexCount(int, int);
    int getIndexCount(int, int);
    void getVertexArray(int, int);
    void getVertexCopy(int, int);
    void getIndexArray(int, int);
    void render(int, int);
    void initWaves();
    void waves_CPU();
    void fps_meter();
    void keyPressEvent(QKeyEvent*) override;

    double _timestep, _x_cam, _z_cam, _pitch, _yaw, _current;
    double _wavenumer[DIRECTIONS];
    double _frequency[DIRECTIONS];
    double _amplitude[DIRECTIONS];
    double _length[DIRECTIONS];
    double _phase[DIRECTIONS];
    QVector2D _wavevector[DIRECTIONS];
    vertexStruct _vertexAll[400];
    QVector2D _h_position;
    QTime fps;
    float _frames = 0;
    float *_vertices;
    float *_vertices_copy;
    int *_indeces;
    float *_normals;
    bool _isInit = false;
    bool _pause = false;
    bool _wire = false;


    }; // class PlaneWidget

#endif
