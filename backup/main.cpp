#include <QApplication>
#include <QVBoxLayout>
#include <GL/glut.h>
#include "mainwindow.h"
#include "widget.h"

int main(int argc, char *argv[])
    { // main()
    // create the application
    QApplication app(argc, argv);


    // create a master widget
        mainwindow *window = new mainwindow(NULL);

    // create a controller to hook things up
    //	GLPolygonController *controller = new GLPolygonController(window, polygon);
    // resize the window
    window->resize(1024, 1224);

    // show the label
    window->show();

    // start it running
    app.exec();

    // clean up
    //	delete controller;

    delete window;

    // return to caller
    return 0;
    } // main()
