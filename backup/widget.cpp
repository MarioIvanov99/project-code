#include <QGLWidget>
#include "widget.h"
#include <iostream>
/*
// Most of the following code is similar to the final version, where it is more thoroughly described.
// The render portion is different, however.
*/



// constructor
widget::widget(QWidget *parent):
  QGLWidget(parent),
  _timestep(0.0), //sets all paramaters to their initial values
  _x_cam(1.0),
  _z_cam(1.0),
  _vertices(0),
  _vertices_copy(0),
  _indeces(0),
  _pitch(0.0),
  _yaw(0.0),
  _h_position()
    { // constructor

    _h_position = QVector2D(0.0, 0.0);

    } // constructor

// called every time the widget is resized
void widget::resizeGL(int w, int h)
    { // resizeGL()
    // set the viewport to the entire widget
    glViewport(0, 0, w, h);


    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);

    gluPerspective(60, width()/height(), 1.0, 200.0);
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);

    } // resizeGL()


int widget::getVertexCount( int width, int height ) {
    return width * height * 3;
}

int widget::getIndexCount( int width, int height ) {
    return ((width-1)*height)*2+width-1;
}


void widget::getVertexArray( int width, int height ) {
    if ( _vertices ) return;

    _vertices = new float[getVertexCount(width, height)];
    int i = 0;

    for ( int j=0; j<height; j++ ) {
        for ( int k=0; k<width; k++ ) {
            _vertices[i] = (float) k;
            i++;
            _vertices[i] = 0.0f;
            i++;
            _vertices[i] = (float) j;
            i++;
        }
    }

}

void widget::getVertexCopy( int width, int height ) {
    if ( _vertices_copy ) return;


    _vertices_copy = new float[getVertexCount(width, height)];
    std::copy(_vertices, _vertices+getVertexCount(width, height), _vertices_copy);


}

void widget::getIndexArray( int width, int height ) {
    if ( _indeces ) return;

    _indeces = new int[getIndexCount(width, height)];
    int temp = 0;
    int q = 0;

    for (int i = 0; i < width - 1; i++) {
        for (int j = 0; j < height; j++) {
            for (int k = 0; k < 2; k++) {
                temp = i + k;
                _indeces[q] = temp * height + j;
                q++;
            }
        }
        _indeces[q] = -1;
        q++;
    }

}




void widget::initWaves(){

    if(_isInit)
        return;

    std::uniform_real_distribution<double> lengthDist(15.0, 50.0);
    std::uniform_real_distribution<double> dirDist(-1.0, 1.0);
    std::uniform_real_distribution<double> freqDist(0.05, 0.3);
    std::uniform_real_distribution<double> ampDist(0.1, 0.15);

    for(int i = 0; i < DIRECTIONS; i++){
        _length[i] = lengthDist(*QRandomGenerator::global());
        _wavenumer[i] = 2*3.14/_length[i];
        _wavevector[i] = QVector2D(dirDist(*QRandomGenerator::global()), dirDist(*QRandomGenerator::global()))*_wavenumer[i];
        _frequency[i] = freqDist(*QRandomGenerator::global());
        _phase[i] = sqrt(9.8/_wavenumer[i]);
        _amplitude[i] = ampDist(*QRandomGenerator::global());

    }

    _isInit = true;

}

void widget::fps_meter(){

    _frames++;
    if(fps.elapsed() >= 1000){
        std::cout<<"FPS: " << _frames << std::endl;
        fps.restart();
        _frames = 0;
    }

}

void widget::waves_CPU(){

    float waveDot;
    //std::cout<<_vertices[2]<<std::endl;

    int k=0;
    double xVal = 0;
    double yVal = 0;
    double zVal = 0;

    for(int i=0;i<HEIGHT*WIDTH;i++){
        _h_position.setX(_vertices[k]);
        _h_position.setY(_vertices[k+2]);
        for(int j=0;j<DIRECTIONS;j++){
            waveDot = QVector2D::dotProduct(_wavevector[j], _h_position);
            xVal = xVal + ((_wavevector[j]/_wavenumer[j]).x())*_amplitude[j]*sin(waveDot - _frequency[j]*_timestep+_phase[j]);
            yVal = yVal + _amplitude[j]*cos(waveDot-_frequency[j]*_timestep+_phase[j]);
            zVal = zVal + ((_wavevector[j]/_wavenumer[j]).y())*_amplitude[j]*sin(waveDot - _frequency[j]*_timestep+_phase[j]);

        }
        _vertices[k] = _vertices_copy[k] - xVal;
        k++;
        _vertices[k] = yVal;
        k++;
        _vertices[k] = _vertices_copy[k] - zVal;
        k++;
        xVal = 0;
        yVal = 0;
        zVal = 0;
    }
}

void widget::render(int width, int height) {


    glEnableClientState( GL_VERTEX_ARRAY ); //Enables the vertex position attribute
    glEnable(GL_PRIMITIVE_RESTART);
    //glPrimitiveRestartIndex(-1);
    glVertexPointer(3, GL_FLOAT, 0, _vertices); //Sets the vertex position data.
    glDrawElements( GL_TRIANGLE_STRIP, widget::getIndexCount(width,height), GL_UNSIGNED_INT, _indeces );
    glDisableClientState( GL_VERTEX_ARRAY );
    glDisable(GL_PRIMITIVE_RESTART);

}


void widget::keyPressEvent(QKeyEvent *event){

    if(event->key() == Qt::Key_F && !event->isAutoRepeat()){
       if(!_wire){
           _wire = true;
           glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
       }else{
           _wire = false;
           glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
       }
    }
    if(event->key() == Qt::Key_P && !event->isAutoRepeat()){
        if(!_pause){
            _pause = true;
            _current = _timestep;
        }else{
            _pause = false;
            _timestep = _current;
        }
    }
}



void widget::updateAngle(){
  _timestep += 1.0;
  this->repaint();
}



void widget::paintGL(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    gluLookAt(-5.0,10.0,-5.0, 0.0,10.0,0.0, 0.0,1.0,0.0);

    QCursor c = cursor();
    c.setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));


    this->getVertexArray(WIDTH,HEIGHT);
    this->getIndexArray(WIDTH,HEIGHT);
    this->getVertexCopy(WIDTH,HEIGHT);


    this->initWaves();
    if(!_pause)
        this->waves_CPU();
    this->render(WIDTH, HEIGHT);
    this->fps_meter();

    glLoadIdentity();

    glFlush();

    }
