
#include "mainwindow.h"

// constructor / destructor
mainwindow::mainwindow(QWidget *parent)
    : QWidget(parent)
    { // constructor

    // create the window layout
    windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);

    // create main widget
    cubeWidget = new widget(this);
    windowLayout->addWidget(cubeWidget);

    ptimer = new QTimer(this);

    ptimer->start(1);

    connect(ptimer, SIGNAL(timeout()),  cubeWidget, SLOT(updateAngle()));


    } // constructor

mainwindow::~mainwindow()
    { // destructor
    delete ptimer;
    delete cubeWidget;
    delete windowLayout;
    } // destructor

// resets all the interface elements
void mainwindow::ResetInterface()
    { // ResetInterface()
    nVerticesSliderY->setMinimum(-3);
    nVerticesSliderY->setMaximum(3);


    //don't use the slider for now

    //	nVerticesSlider->setValue(thePolygon->nVertices);

    // now force refresh
    cubeWidget->update();
    update();
    } // ResetInterface()
