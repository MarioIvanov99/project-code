#version 330 core

in vec3 light_direction;
in vec3 normal_camera;
in vec3 eye_direction;
in vec2 UV;

out vec4 color;

uniform sampler2D reflectiontex;

void main(){

	
  	color = texture(reflectiontex, UV); //Sets the color of the surface to the texture
  	color.a = 0.7; //Sets the alpha to be slightly transparent
  	//color = vec4(0.0, 0.0, 0.0, 1.0); //Used for testing
  
}
