#include <GL/glu.h>
#include <QGLWidget>
#include "widget.h"
#include <iostream>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>



// constructor
widget::widget(QWidget *parent):
  QGLWidget(parent),
  _timestep(0.0) //sets all paramaters to their initial values
    { // constructor

        _move = {false,false,false,false}; //Sets the movement to false in all directions.

        _h_position = QVector2D(0.0, 0.0); //Sets the horizontal position. Only used for the CPU implementation. Not actually used in this iteration of the project.

    } // constructor

widget::~widget()
    { // destructor

        //Frees the memory from all the pointers used
        free(_vertices);
        free(_vertices_copy);
        free(_indeces);
        free(_indeces2);

    } // destructor

// called when OpenGL context is set up
void widget::initializeGL(){

    initializeOpenGLFunctions(); //Initializes and allows the use of OpenGL functions, as implemented through the respective QT library.

    fps.start(); //Starts the timer for measuring fps

    //Enables the alpha portion of colors in shaders.
    //Turns the output of the fragment shader from a 3D vector to a 4D vector
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    this->createFBO(); //Creates a frame buffer

    //Stores the vertex data into the their respective arrays
    //The arhuments are the dimensions of the surface, the position in the world, where the surface starts and the direction it faces.
    _vertices = this->getVertexArray(WIDTH, HEIGHT, 0.0f, 0.0f, 0.0f, 0);
    _vertices_copy = this->getVertexArray(WIDTH/10, HEIGHT/10, 100.0f, 10.0f, 100.0f, 1);

    //Stores the index data into the their respective arrays
    _indeces = this->getIndexArray(WIDTH,HEIGHT);
    _indeces2 = this->getIndexArray(WIDTH/10, HEIGHT/10);

    //Create surface to be reflected
    glGenVertexArrays(1, &_vao2); //Generate and bind 1 vertex array,
    glBindVertexArray(_vao2);
    glGenBuffers(1, &_vertexbuffer2); //Generate and bind 1 vertex buffer, where vertex data of appropriate size is stored,
    glBindBuffer(GL_ARRAY_BUFFER, _vertexbuffer2);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float)*getVertexSize(WIDTH/10, HEIGHT/10), _vertices_copy, GL_STATIC_DRAW);
    glGenBuffers(1, &_indexbuffer2); //Generate and bind 1 vertex buffer, where index data of appropriate size is stored,
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexbuffer2);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*getIndexSize(WIDTH/10, HEIGHT/10), _indeces2, GL_STATIC_DRAW);

    //The same process is repeated for every separate object that needs to be put into the scene.
    glGenVertexArrays(1, &_vao);
    glBindVertexArray(_vao);
    glGenBuffers(1, &_vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float)*getVertexSize(WIDTH, HEIGHT), _vertices, GL_STATIC_DRAW);
    glGenBuffers(1, &_indexbuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexbuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*getIndexSize(WIDTH, HEIGHT), _indeces, GL_STATIC_DRAW);

    //The projection matrix is set using perspective. It does not change
    _pMatrix.perspective(45.0f, (float)width()/(float)height(), 1.0f, 4000.0f);

    //The initial values for the view matrices. Orientated in such a way that the initial direction of the first render's camera lines up with the final render's camera.
    _vMatrix.lookAt(QVector3D(HEIGHT/2,200,-50),QVector3D(WIDTH/2,0,HEIGHT/2),QVector3D(0,1,0));
    _vMatrix3.lookAt(QVector3D(HEIGHT/2,200,-50),QVector3D(WIDTH/2,0,HEIGHT/2),QVector3D(0,1,0));
    _vMatrix2.lookAt(QVector3D(HEIGHT/2,-200,-50),QVector3D(WIDTH/2,0,HEIGHT/2),QVector3D(0,1,0));

    //Model matrix. Just set to the identity matrix.
    _mMatrix.setToIdentity();

    //Initial mvp matrix
    _mvp = _pMatrix*_vMatrix*_mMatrix;

    setMouseTracking(true); //Sets mouse tracking.
    QCursor c = cursor(); //Creates cursor object.
    c.setPos(mapToGlobal(QPoint(width() / 2, height() / 2))); //Sets the initial position of the cursor to the centre of the window.
    c.setShape(Qt::BlankCursor); //Makes the cursor transparent.
    setCursor(c);
    setFocusPolicy(Qt::StrongFocus); //Makes it so mouse movement without is tracked without the need for a left click.

}

std::string widget::shaderToString(std::string file){ //Gets a file and puts its contents into a string to act as shader code

    std::ifstream shaderFile(file); //Takes a file as input.
    std::string str;

    if(shaderFile){

       std::ostringstream currentChar;
       currentChar << shaderFile.rdbuf(); //Reads every character into the string.
       str += currentChar.str(); //Adds character to final string.

    }

    return str; //Returns shader code.

}

void widget::createFBO(){ //Creates a second frame buffer (the first being the default).

    glGenFramebuffers(1, &_frameBuffer); //Generates and bind a frame buffer
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);

    //Generates and binds a texture, and attaches componenets to it.
    glGenTextures(1, &_texture);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024, 1024, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    //Generates and binds a render buffer.
    //Since the render buffer is not bound and unbound, it is declared here.
    //Render buffer is just created for frame buffer completeness.
    GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 1024, 1024);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);

    //Attaches the texture to the frame buffer.
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _texture, 0);

    //Checks if the frame buffer is complete.
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "Framebuffer is not complete!" << std::endl;


}

unsigned int widget::compileShader(unsigned int type, const std::string& source){

    GLuint shader_comp = glCreateShader(type); //Creates a shader.

    const char* shader = source.c_str(); //Shader requires a pointer to be compiled.

    glShaderSource(shader_comp, 1, &shader, NULL); //Sets the source of the shader and compiles it
    glCompileShader(shader_comp);

    return shader_comp; //Returns shader component

}

int widget::createShader(const std::string& vert, const std::string& frag){

    //Creates a shader program and adds components to it.
    GLuint program = glCreateProgram();
    GLuint vertex = compileShader(GL_VERTEX_SHADER, vert);
    GLuint fragment = compileShader(GL_FRAGMENT_SHADER, frag);

    //Attaches a completes the program.
    glAttachShader(program, vertex);
    glAttachShader(program, fragment);
    glLinkProgram(program);
    glValidateProgram(program);

    //Deletes the components, as they are no longer required.
    glDeleteShader(vertex);
    glDeleteShader(fragment);

    return program; //Returns completed shader program.

}

//Called every time the widget is resized.
void widget::resizeGL(int w, int h)
    {
    //Sets the viewport to the entire widget.
    glViewport(0, 0, w, h);

    glEnable(GL_TEXTURE_2D);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);

    }


int widget::getVertexSize( int width, int height ){

    return width * height * 3; //Returns the vertex array size. Three because each vertex contains three floats.

}


int widget::getIndexSize( int width, int height ){

    return ((width-1)*height)*2+width-1; //Returns the size of the index array. The addition of width+1 is for the restart indices.

}


float *widget::getVertexArray( int width, int height, float xoffset, float yoffset, float zoffset, int direction ){ //Fills the vertex array with data

    int i = 0;

    float *vertex = new float[getVertexSize(width, height)]; //Allocates an appropriate amount of memory.

    for ( int j=0; j<height; j++ ){ //Loops for height*width.

        for ( int k=0; k<width; k++ ){

            switch (direction){ //Creates the surface in a specific direction

              case 1: //Faces x
                vertex[i] = xoffset;
                i++;
                vertex[i] = (float) k+yoffset;
                i++;
                vertex[i] = (float) j+zoffset;
                i++;
                break;
              case 2: //Faces z
                vertex[i] = (float) k+xoffset;
                i++;
                vertex[i] = (float) j+yoffset;
                i++;
                vertex[i] = zoffset;
                i++;
                break;
              default: //Faces y
                vertex[i] = (float) k+xoffset;
                i++;
                vertex[i] = yoffset;
                i++;
                vertex[i] = (float) j+zoffset;
                i++;
                break;

            }

        }

    }

    return vertex;

}



int *widget::getIndexArray( int width, int height ){

    int *indices = new int[getIndexSize(width, height)]; //Allocates the appropriate amount of memory.

    int temp = 0;
    int q = 0; //Used as an index for the index array.

    for (int i = 0; i < width - 1; i++){

        for (int j = 0; j < height; j++){

            for (int k = 0; k < 2; k++){ //This is used because triangles have indices on two different rows. This is also the reason the outermost for loop is only until width-1.

                temp = i + k;
                indices[q] = temp * height + j;
                q++;

            }

        }

        indices[q] = -1; //Primitive restart index. Used to prevent artifacts. Negative so as to not conflict with the contents of the array.
        q++;

    }

    return indices;

}



void widget::initWaves(){ //Initiates the waves

    if(_isInit) //Checks whether waves are already initialized.
        return;

    //Creates a distribution for each wave property.
    //This is done in order to have random real numbers.
    std::uniform_real_distribution<double> lengthDist(4.0, 10.0);
    std::uniform_real_distribution<float> dirDist(-1.0, 1.0);
    std::uniform_real_distribution<double> freqDist(0.05, 0.2);
    std::uniform_real_distribution<double> ampDist(0.001, 0.1);

    for(int i = 0; i < DIRECTIONS; i++){//Looped for DIRECTIONS.

        _length[i] = lengthDist(*QRandomGenerator::global());
        _wavenumer[i] = 2*PI/_length[i]; //No random generation for wavenumber. The formula is just 2*pi/wavelength.
        _frequency[i] = freqDist(*QRandomGenerator::global());
        _phase[i] = sqrt(9.8/_wavenumer[i]); //The phase is also not random.
        _amplitude[i] = ampDist(*QRandomGenerator::global());

    }

    for(int i = 0; i < DIRECTIONS*2; i++){//Wave vector is just direction multiplied by wave number.
        _wavevector[i] = dirDist(*QRandomGenerator::global())*_wavenumer[i/2];
    }

    _isInit = true; //Sets its initialization value to true.

}

/*void widget::waves_CPU(){ //Example CPU implementation of Gerstner waves.

    float waveDot;
    //std::cout<<_vertices[2]<<std::endl;

    int k=0;
    double xVal = 0;
    double yVal = 0;
    double zVal = 0;

    for(int i=0;i<HEIGHT*WIDTH;i++){
        _h_position.setX(_vertices[k]);
        _h_position.setY(_vertices[k+2]);
        for(int j=0;j<DIRECTIONS;j++){
            waveDot = QVector2D::dotProduct(_wavevector[j], _h_position);
            xVal = xVal + ((_wavevector[j]/_wavenumer[j]).x())*_amplitude[j]*sin(waveDot - _frequency[j]*_timestep+_phase[j]);
            yVal = yVal + _amplitude[j]*cos(waveDot-_frequency[j]*_timestep+_phase[j]);
            zVal = zVal + ((_wavevector[j]/_wavenumer[j]).y())*_amplitude[j]*sin(waveDot - _frequency[j]*_timestep+_phase[j]);

        }
        _vertices[k] = _vertices_copy[k] - xVal;
        k++;
        _vertices[k] = yVal;
        k++;
        _vertices[k] = _vertices_copy[k] - zVal;
        k++;
        xVal = 0;
        yVal = 0;
        zVal = 0;
    }
}*/




void widget::mouseMoveEvent(QMouseEvent *event){

    //Value for yaw and pitch update each time the mouse is moved.
    //Divide by a large number in order for the movement to not be too fast.
    _yaw += ((float)width()/2-event->pos().x())/8000.0;
    _pitch += ((float)height()/2-event->pos().y())/8000.0;


}

void widget::keyPressEvent(QKeyEvent *event){

    //Movement is blocked if the camera is moving in the opposite direction.
    if(event->key() == Qt::Key_W && !_move.bck)
       _move.fwd = true;
    if(event->key() == Qt::Key_S && !_move.fwd)
       _move.bck = true;
    if(event->key() == Qt::Key_D && !_move.lft)
       _move.rgt = true;
    if(event->key() == Qt::Key_A && !_move.rgt)
       _move.lft = true;

    //Toggles fill and wireframe mode
    if(event->key() == Qt::Key_F && !event->isAutoRepeat()){ //isAutoRepeat requiring false means that holding down the button won't just quickly toggle between the two.
       if(!_wire){
           _wire = true;
           glPolygonMode( GL_FRONT_AND_BACK, GL_LINE ); //GL_LINE = Wireframe
       }else{
           _wire = false;
           glPolygonMode( GL_FRONT_AND_BACK, GL_FILL ); //GL_FILL = Fill
       }
    }

    //Pauses water animation.
    //Does not pause the scene. Camera can still move and so can the reflection.
    if(event->key() == Qt::Key_P && !event->isAutoRepeat()){
        if(!_pause){

            _pause = true;
            _current = _timestep; //The timestep is stored so that the animation can continue from where it left off, when it is unpaused.

        }else{

            _pause = false;
            _timestep = _current;
        }

    }

}

void widget::keyReleaseEvent(QKeyEvent *event){

    //Releasing sets movement to false, so opposite direction can be pressed.
    //isAutoRepeat is set to false so that holding down the button doesn't rapidly trigger the key release event.
     if(event->key() == Qt::Key_W && !event->isAutoRepeat())
               _move.fwd = false;
     if(event->key() == Qt::Key_S && !event->isAutoRepeat())
               _move.bck = false;
     if(event->key() == Qt::Key_D && !event->isAutoRepeat())
               _move.rgt = false;
     if(event->key() == Qt::Key_A && !event->isAutoRepeat())
               _move.lft = false;

}


void widget::camera(){


    if(_pitch >= 1) //Sets the vertical angle to 90 degress top and bottom.
          _pitch = 1;
      if(_pitch <= -1)
          _pitch = -1;

      //Gets the direction in which the camera is looking.
      _direction.setX(cos(_pitch)*sin(_yaw));
      _direction.setY(sin(_pitch)); //The vertical position, y, is sin instead of -sin so as to not be have inverted movement.
      _direction.setZ(cos(_pitch)*cos(_yaw));

      //Gets the angle between two vectors. dot(a, b)/|a|*|b|
      float angleDot = QVector3D::dotProduct(_direction, _previous_direction); //Gets the dot between current and previous direction.

      float mag1 = sqrt(pow(_direction.x(), 2) + pow(_direction.y(), 2)+ pow(_direction.z(), 2)); //Gets the magnitude of the current direction vector.
      float mag2 = sqrt(pow(_previous_direction.x(), 2) + pow(_previous_direction.y(), 2)+ pow(_previous_direction.z(), 2)); //Gets the magnitude of the previous direction vector.

      int sign = 0; //Sign for rotation

      if(_prev_yaw>_yaw) //Sign is dependant on whether the mouse was moved left or right.
          sign = -1;
      if(_prev_yaw<_yaw)
          sign = 1;

      _camera_angle = sign*(acos((angleDot/(mag1*mag2))))*180.0/PI; //Final result of angle between the two directions

      if(!isnanf(_camera_angle)) //Checks if the angle is nan. The angle is nan during initialization. This is done so that _rotate_angle doesn't get set to nan.
        _rotate_angle = _rotate_angle + _camera_angle; //Rotation angle is the total rotation from the original direction. This is done because the mvp matrix of the first render is declared every render.

      _previous_direction = _direction;
      _prev_yaw=_yaw;

      //Changes the position vector.
      //Forward and Backward only use x and z, so that the y position does not change.
      //For the movement left and right, what is added represents a vector pointing left.
      //Direction of right is only dependant on the horizontal direction.
      if(_move.fwd){
          _position.setX(_position.x()+_direction.x());
          _position.setZ(_position.z()+_direction.z());
      }
      if(_move.bck){
          _position.setX(_position.x()-_direction.x());
          _position.setZ(_position.z()-_direction.z());
      }
      if(_move.lft){
          _position += QVector3D(sin(_yaw+PI/2.0), 0, cos(_yaw+PI/2.0));
      }
      if(_move.rgt){
          _position -= QVector3D(sin(_yaw+PI/2.0), 0, cos(_yaw+PI/2.0));
      }

      //The view matrix is reset and then set to the new values.
      //The direction in which the camera looks at is position+direction.
      //This acts as if the view is translated to _position and is then set to look at direction.
      _vMatrix.setToIdentity();
      _vMatrix.lookAt(_position, _position+_direction, QVector3D(0,1,0));

}

void widget::fps_meter(){

    _frames++; //Increments frame every time the function is called
    if(fps.elapsed() >= 1000){ //If one second has passed, displays the frames, resets the timer and resets the frame count.

        std::cout<<"FPS: " << _frames << std::endl;
        fps.restart();
        _frames = 0;

    }

}

void widget::updateTime(){ //Increments time by 1 every time the timer send a signal. It also calls PaintGL.

  _timestep += 1.0;
  this->repaint();

}

void widget::render_surface(){ //Renders the water surface

    glBindVertexArray(_vao);
    glEnableVertexAttribArray(0); //Enables attribute 0, which usually correlaes to position
    glBindBuffer(GL_ARRAY_BUFFER, _vertexbuffer);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    //glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexbuffer);
    glEnable(GL_PRIMITIVE_RESTART); //Primitive restart resets the index order for each triangle after each row.
    glPrimitiveRestartIndex(-1);
    //Draw the surface.
    //Triangle strip instead of Triangles because it takes less space.
    glDrawElements(GL_TRIANGLE_STRIP, getIndexSize(WIDTH, HEIGHT), GL_UNSIGNED_INT, (void*)0);
    //Disables used functions.
    glDisableVertexAttribArray(0);
    glDisable(GL_PRIMITIVE_RESTART);

}

void widget::render_surface2(){ //Renders the reflected surface using the same principles as above

    glBindVertexArray(_vao2);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexbuffer2);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indexbuffer2);
    glEnable(GL_PRIMITIVE_RESTART);
    glPrimitiveRestartIndex(-1);
    glDrawElements(GL_TRIANGLE_STRIP, getIndexSize(WIDTH/10, HEIGHT/10), GL_UNSIGNED_INT, (void*)0);
    glDisableVertexAttribArray(0);
    glDisable(GL_PRIMITIVE_RESTART);

}



void widget::paintGL(){ //Draws the scene

    if(_pause){ //Used to pause animation
        _timestep = _current;
    }

    this->camera();

    if(_shaderprogram == 0){ //Creates a shader program and send relevant uniform data to it
        _shaderprogram = this->createShader(this->shaderToString("vertShader.shader"), this->shaderToString("fragShader.shader"));
        _MVP_uniform = glGetUniformLocation(_shaderprogram, "MVP");
        _MVP_reflection = glGetUniformLocation(_shaderprogram, "MVP2");
        _time_uniform = glGetUniformLocation(_shaderprogram, "timestep");
        _wavevector_uniform = glGetUniformLocation(_shaderprogram, "wavevector");
        _wavenumber_uniform = glGetUniformLocation(_shaderprogram, "wavenumber");
        _frequency_uniform = glGetUniformLocation(_shaderprogram, "frequency");
        _amplitude_uniform = glGetUniformLocation(_shaderprogram, "amplitude");
        _phase_uniform = glGetUniformLocation(_shaderprogram, "phase");
        _direction_uniform = glGetUniformLocation(_shaderprogram, "directions");
        _texture_uniform = glGetUniformLocation(_shaderprogram, "reflectiontex");
    }

    if(_cubeprogram == 0){ //Creates a shader program and send relevant uniform data to it
         _cubeprogram = this->createShader(this->shaderToString("CubeV.shader"), this->shaderToString("CubeF.shader"));
         _MVP_uniform2 = glGetUniformLocation(_cubeprogram, "MVP");
    }

    this->initWaves();

    QCursor c = cursor(); //Sets the location of the cursor every time the scene is drawn.
    c.setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));

    //Declares mvp matrices every time the scene is rendered.
    //This is done because they need to be updated, but QMatrix4x4 objects cannot be reassinged.
    QMatrix4x4 mvp_matrix, mvp_matrix2, mvp_matrix3;

    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer); //Binds the frame buffer for reflection, meaning that everything drawn is drawn to that frame buffer.
    glViewport(0,0,1024,1024);
    glClearColor(0.0,0.0,0.3,0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //Clears the scene each render.

    mvp_matrix = _pMatrix*_vMatrix2*_mMatrix; // Rotates the view around the centre of the surface
    mvp_matrix.translate(WIDTH/2, 0.0, HEIGHT/2);
    mvp_matrix.rotate(-_rotate_angle, 0.0, 1.0, 0.0);
    mvp_matrix.translate(-WIDTH/2, 0.0, -HEIGHT/2);


   glUseProgram(_cubeprogram); //Uses the reflected surface texture to draw. This is a simpler shader just used for testing
   glUniformMatrix4fv(_MVP_uniform2, 1, GL_FALSE, &mvp_matrix.data()[0]);

   glBindTexture(GL_TEXTURE_2D, 0);
   this->render_surface2();

   glBindFramebuffer(GL_FRAMEBUFFER, 0); //Binds the default frame buffer
   //glDisable(GL_DEPTH_TEST);
   glViewport(0,0,1024,1024);
   glClearColor(0.6,0.6,0.6,0.0);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   mvp_matrix2 = _pMatrix*_vMatrix*_mMatrix;
   mvp_matrix3 = _pMatrix*_vMatrix3*_mMatrix;

   mvp_matrix3.translate(WIDTH/2, 0.0, HEIGHT/2);
   mvp_matrix3.rotate(-_rotate_angle, 0.0, 1.0, 0.0);
   mvp_matrix3.translate(-WIDTH/2, 0.0, -HEIGHT/2);

   glUniformMatrix4fv(_MVP_uniform2, 1, GL_FALSE, &mvp_matrix2.data()[0]);
   this->render_surface2(); //The shader for this object is already set, so it is just rendered.

    //The water surface shader and the reflection texture are set.
    glUseProgram(_shaderprogram);
    glBindTexture(GL_TEXTURE_2D, _texture);

    //All relevent uniforms are sent to the sahder
    glUniformMatrix4fv(_MVP_uniform, 1, GL_FALSE, &mvp_matrix2.data()[0]);
    glUniformMatrix4fv(_MVP_reflection, 1, GL_FALSE, &mvp_matrix3.data()[0]);
    glUniform2fv(_wavevector_uniform, DIRECTIONS, &_wavevector[0]);
    glUniform1fv(_wavenumber_uniform, DIRECTIONS, &_wavenumer[0]);
    glUniform1f(_time_uniform, _timestep);
    glUniform1fv(_phase_uniform, DIRECTIONS, &_phase[0]);
    glUniform1fv(_amplitude_uniform, DIRECTIONS, &_amplitude[0]);
    glUniform1fv(_frequency_uniform, DIRECTIONS, &_frequency[0]);
    glUniform1i(_texture_uniform, 0);
    glUniform1f(_direction_uniform, directions);



    glLoadIdentity();

    this->render_surface();
    this->fps_meter(); //The FPS is measured at the end of rendering.

    glFlush();

    }
