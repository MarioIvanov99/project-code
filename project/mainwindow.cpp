#include "mainwindow.h"

// constructor / destructor
mainwindow::mainwindow(QWidget *parent)
    : QWidget(parent)
    { // constructor

    // create the window layout
    windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);

    // create main widget
    cubeWidget = new widget(this);
    windowLayout->addWidget(cubeWidget);

    ptimer = new QTimer(this);

    ptimer->start(50); //The interval, in milliseconds, at which the timer sends a signal.

    connect(ptimer, SIGNAL(timeout()),  cubeWidget, SLOT(updateTime())); //Send a time signal to the updateTime function.


    } // constructor

mainwindow::~mainwindow()
    { // destructor
    delete ptimer;
    delete cubeWidget;
    delete windowLayout;
    } // destructor

// resets all the interface elements
void mainwindow::ResetInterface()
    { // ResetInterface()

    cubeWidget->update();
    update();
    } // ResetInterface()
