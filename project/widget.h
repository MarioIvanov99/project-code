#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget> //Used for all function related to widgets in Qt.

#include <QOpenGLFunctions_4_5_Core> //The library that Qt provides for OpenGL functions. Specific version chosen because of the need for primitive restart.
#include <QRandomGenerator> //Used for the initialization of the wave properties.
#include <QMatrix4x4> //Used for the the model, view and projection matrices.
#include <QTime> //Used for fps measurements.

#include <QKeyEvent> //Used for handling keyboard presses.
#include <QMouseEvent> //Used for handling mouse movement.

#include <string>
//Both used to get strings from files
#include <sstream>
#include<fstream>

#define PI 3.14159265359
//These three predefined variables are used to alter the size of the calculations.
//They are used throughout the program
#define HEIGHT 300
#define WIDTH 300
#define DIRECTIONS 50

typedef struct motion { //A struct of bools used for motion.

  bool fwd, bck, lft, rgt;

} motion;

class widget: public QGLWidget, protected QOpenGLFunctions_4_5_Core //The widget class uses functions from the two libraries.
    {

    Q_OBJECT

    public:
    widget(QWidget *parent); //Constructor for the widget
    ~widget(); //Destructor for the widget

    public slots:
    //called by the timer in the main window
    void updateTime(); //Is a slot, which takes in a timer signal to update the timestep.



    protected:

    // called when OpenGL context is set up
    void initializeGL() override;
    // called every time the widget is resized
    void resizeGL(int w, int h) override;
    // called every time the widget needs painting
    void paintGL() override;


    private:

    int getVertexSize(int, int); //Gets the number of elements in the vertex array.
    float *getVertexArray(int, int, float, float, float, int); //Fills a pointer with the position data of the surface.

    int getIndexSize(int, int); //Gets the number of elements in the vertex array.
    int *getIndexArray(int, int); //Fills the index array of the surface.

    void initWaves(); //Sets the values for all waves. Called once.

    void waves_CPU(); //The Gerstner model adapted to a function running on the CPU.

    void mouseMoveEvent(QMouseEvent*) override; //Handles mouse events.
    void keyPressEvent(QKeyEvent*) override; //Handles press events.
    void keyReleaseEvent(QKeyEvent*) override; //Handles release events.

    void camera(); //Defines the camera

    //Various functions used to get a shader, in the form of a string, from a file and compile it.
    unsigned int compileShader(unsigned int, const std::string&);
    int createShader(const std::string&, const std::string&);
    std::string shaderToString(std::string);

    void render_surface(); //Renders the surface that represents the water surface.
    void render_surface2(); //Render a test surface to be reflected.

    void createFBO(); //Creates a complete frame buffer object.
    void fps_meter(); //Used to measure fps.

    float _frames = 0; //Counts frames in a second.

    //_timestep is the time, as incremented by the timer.
    //_pitch and _yaw are the euler angles used for the camera.
    //_prev_yaw used when calculating the rotation of the reflection.
    //_current stores the current time. Used for pausing.
    float _timestep, _x_cam, _z_cam, _pitch, _yaw, _prev_yaw = 0, _current = 0;

    //Every wave property being declared as static arrays of size DIRECTIONS.
    float _wavenumer[DIRECTIONS];
    float _frequency[DIRECTIONS];
    float _amplitude[DIRECTIONS];
    float _length[DIRECTIONS];
    float _phase[DIRECTIONS];
    float _wavevector[2*DIRECTIONS];
    QVector2D _waveproto[DIRECTIONS];

    QVector2D _h_position; //Only used for the CPU implementation.

    float *_vertices; //Stores data of water surface vertex positions.
    float *_vertices_copy; //Stores data of reflected surface vertex positions.

    int *_indeces; //Stores data of water surface indeces.
    int *_indeces2; //Stores data of reflected surface indeces.

    motion _move; // Movement bools.

    bool _isInit = false; //Checks if waves are initialized.

    //Used for the toggles for pausing and wireframe.
    bool _pause = false;
    bool _wire = false;

    float directions = DIRECTIONS; //Used to send direction data to the shader.

    //The various undsigned integers represent buffers or textures that need to be bound and unbound throughout the program and uniforms.
    GLuint _frameBuffer; //Frame buffer for reflection.
    GLuint _texture; //Texture for reflection.

    GLuint _vao; //Vertex array object for the water surface.
    GLuint _vao2; //Vertex array object for the reflected surface.

    GLuint _vertexbuffer; //Vertex buffer object for the position attribute of the water surface.
    GLuint _vertexbuffer2; //Vertex buffer object for the position attribute of the reflected surface.
    GLuint _indexbuffer; //Vertex buffer object for the indices of the water surface.
    GLuint _indexbuffer2; //Vertex buffer object for the indices  of the reflected surface.

    GLuint _shaderprogram = 0; //Shader for the water surface.
    GLuint _cubeprogram = 0; //Shader for the reflected surface.

    GLuint _MVP_uniform = 0; //Sends mvp matrix to the water surface's shader.
    GLuint _MVP_uniform2 = 0; //Sends mvp matrix to the reflected surface's shader.
    GLuint _MVP_reflection = 0; //Sends reflection mvp data to the water surface's shader.

    GLuint _M_uniform = 0;
    GLuint _V_uniform = 0;
    GLuint _P_uniform = 0;

    //Send wave property data to the water surface's shader.
    GLuint _time_uniform = 0;
    GLuint _frequency_uniform = 0;
    GLuint _wavenumber_uniform = 0;
    GLuint _wavevector_uniform = 0;
    GLuint _phase_uniform = 0;
    GLuint _amplitude_uniform = 0;
    GLuint _texture_uniform = 0;
    GLuint _direction_uniform = 0;

    QMatrix4x4 _pMatrix; //Projection matrix.
    QMatrix4x4 _vMatrix; //View matrix for water surface.
    QMatrix4x4 _vMatrix2; //View matrix for reflected surface.
    QMatrix4x4 _vMatrix3; //View matrix for reflection texture.
    QMatrix4x4 _mMatrix; //Model matrix.
    QMatrix4x4 _mvp; //Completed matrix.

    QVector3D _position = QVector3D(0, 10, 0); //Position of the camera.
    QVector3D _direction = QVector3D(0, 0, 0); //Direction of the camera.
    QVector3D _previous_direction = QVector3D(0, 10, 0); //Used for the rotation of the reflection.

    QTime fps; //Used to check if 1 second has passed.

    float _camera_angle = 0; //Defines how much the camera is rotated, when the mouse is moved.
    float _rotate_angle = 0; //Defines the total rotation of the reflection.


    };

#endif
