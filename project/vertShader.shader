#version 330 core

layout(location = 0) in vec3 position;

out vec2 UV; //Output to the fragment shader

uniform float directions;
uniform mat4 MVP;
uniform mat4 MVP2;
uniform float wavenumber[200];
uniform float frequency[200];
uniform float phase[200];
uniform float amplitude[200];
uniform float timestep;
uniform vec2 wavevector[200];

void main()
{
	
	float xVal = 0;
	float yVal = 0;
	float zVal = 0;
	float waveDot;
	vec2 h_position; //Horizontal position
	h_position.x = position.x;
	h_position.y = position.z;
	
	
	//Sets normalized device coordinates
	UV.x = (MVP2 * vec4(position, 1)).x/(MVP2 * vec4(position, 1)).w/2.0+0.5;
	UV.y = -(MVP2 * vec4(position, 1)).y/(MVP2 * vec4(position, 1)).w/2.0+0.5;
	
	
	for(int i = 0; i<directions; i++){ //Gerstner equation for waves
		waveDot = dot(wavevector[i], h_position);
		xVal = xVal + ((wavevector[i]/wavenumber[i]).x)*amplitude[i]*sin(waveDot-frequency[i]*timestep+phase[i]);
		yVal = yVal + amplitude[i]*cos(waveDot-frequency[i]*timestep+phase[i]);
		zVal = zVal + ((wavevector[i]/wavenumber[i]).y)*amplitude[i]*sin(waveDot-frequency[i]*timestep+phase[i]);
	}
	
	vec3 temp_pos; 
	temp_pos.x = position.x-xVal;
	temp_pos.y = position.y-yVal;
	temp_pos.z = position.z-zVal;
	
	
	gl_Position = MVP * vec4(temp_pos, 1);
	//gl_Position = MVP * vec4(position, 1); //Used for testing
	
	
}
